<?php
use App\Http\Controllers\Helper\MAC;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes();

Route::get('/mlogin', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/mlogin', 'Auth\LoginController@login');
// Route::get('/mregister', 'Auth\RegisterController@showRegistrationForm')->name('register');
// Route::post('/mregister/', 'Auth\RegisterController@register');
// Route::get('/mpassword/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
// Route::post('/mpassword/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
// Route::get('/mpassword/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
// Route::post('/mpassword/reset', 'Auth\ResetPasswordController@reset');
Route::get('/logout', 'AuthController@logout')->name('logout');

include base_path('routes/admin.php');
// include base_path('routes/view.php');



Route::get('/', 'Website\HomeController@index')->name('home');
Route::get('su-kien/du-doan-ty-so-viet-nam-malaysia.html', 'Website\HomeController@event');
Route::get('su-kien/du-doan-ket-qua-viet-nam-malaysia.html', 'Website\HomeController@bongda');
Route::get('su-kien/du-doan-ket-qua-viet-nam-indo.html', 'Website\HomeController@vnindo');
// Route::get('/hosting.html', 'Website\HomeController@hosting');
// Route::get('/vps.html', 'Website\HomeController@hosting');
// Route::get('/dedicated.html', 'Website\HomeController@hosting');
// Route::get('/tu-rack.html', 'Website\HomeController@hosting');
Route::get('/2019/04/17/anti-ddos-la-gi', function() {
    return redirect('https://www.nps.vn/kb/blogs/kien-thuc/anti-ddos/dos-ddos-la-gi-cach-chong-hacker-ddos-nhu-the-nao.html');
});

Route::get('/{slug}.html', 'Website\HomeController@page');
// Route::get('/sitemapnps.xml', 'Website\HomeController@xml');
// $segment = '';
// $page = Request::segment(1);
// if (!empty($page)) {
// 	$segment = MAC::redirect_page($page);

// 	if ($segment->status == 200) {
// 		dd($page);
// 		Route::get('/'.$page, 'Website\HomeController@page')->name('page');
// 	}else{
// 		Route::get('/404');
// 	}
// }else{
// 	Route::get('/404');
// }



