<?php
return [
	// [
	// 	'name' 			=> 'Trang quản trị',
	// 	'url' 			=> 'dashboard',
	// 	'segment'		=> 'dashboard',
	// 	'icon'			=> 'fab fa-windows',
	// 	'description'	=> ''
	// ],
	// [
	// 	'name' 			=> 'Quản lý người dùng',
	// 	'url' 			=> 'users',
	// 	'segment'		=> 'users',
	// 	'icon'			=> 'fas fa-users',
	// 	'description'	=> ''
	// ],
	[
		'name' 			=> 'Quản lý quản trị viên',
		'url' 			=> 'administrators',
		'segment'		=> 'administrators',
		'icon'			=> 'fas fa-users-cog',
		'description'	=> ''
	],
	[
		'name' 			=> 'Trang',
		'url' 			=> 'pages',
		'segment'		=> 'pages',
		'icon'			=> 'fas fa-list-ol',
		'description'	=> ''
	],
	// [
	// 	'name' 			=> 'Danh mục bài viết',
	// 	'url' 			=> 'category',
	// 	'segment'		=> 'category',
	// 	'icon'			=> 'fas fa-list-ol',
	// 	'description'	=> ''
	// ],
	// [
	// 	'name' 			=> 'Bài viết',
	// 	'url' 			=> 'posts',
	// 	'segment'		=> 'posts',
	// 	'icon'			=> 'fas fa-pen-nib',
	// 	'description'	=> ''
	// ],
	[
		'name' 			=> 'Danh mục sản phẩm',
		'url' 			=> 'catalog',
		'segment'		=> 'catalog',
		'icon'			=> 'fas fa-th-list',
		'description'	=> ''
	],
	// [
	// 	'name' 			=> 'Sản phẩm',
	// 	'url' 			=> 'products',
	// 	'segment'		=> 'products',
	// 	'icon'			=> 'fas fa-boxes',
	// 	'description'	=> ''
	// ],
	[
		'name' 			=> 'Hosting',
		'url' 			=> 'hosting',
		'segment'		=> 'hosting',
		'icon'			=> 'fas fa-coins',
		'description'	=> ''
	],
	[
		'name' 			=> 'VPS',
		'url' 			=> 'vps',
		'segment'		=> 'vps',
		'icon'			=> 'fas fa-cloud',
		'description'	=> ''
	],
	[
		'name' 			=> 'Server/ dedicated',
		'url' 			=> 'dedicated',
		'segment'		=> 'dedicated',
		'icon'			=> 'fas fa-server',
		'description'	=> ''
	],
	[
		'name' 			=> 'NWall',
		'url' 			=> 'nwall',
		'segment'		=> 'nwall',
		'icon'			=> 'fab fa-gripfire',
		'description'	=> ''
	],
	[
		'name' 			=> 'Tủ Rack',
		'url' 			=> 'rack',
		'segment'		=> 'rack',
		'icon'			=> 'fas fa-inbox',
		'description'	=> ''
	],
	[
		'name' 			=> 'Chỗ đặt Server',
		'url' 			=> 'colocation',
		'segment'		=> 'colocation',
		'icon'			=> 'fas fa-map-marker-alt',
		'description'	=> ''
	],
	[
		'name' 			=> 'SEO',
		'url' 			=> 'seo',
		'segment'		=> 'seo',
		'icon'			=> 'fas fa-chart-line',
		'description'	=> ''
	],
	[
		'name' 			=> 'Cài đặt',
		'url' 			=> 'config',
		'segment'		=> 'config',
		'icon'			=> 'fas fa-cogs',
		'description'	=> ''
	],
];