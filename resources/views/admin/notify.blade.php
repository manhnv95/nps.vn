@if ($errors->any())
	@foreach ($errors->all() as $error)
		<div class="alert alert-danger alert-dismissible fade show mt-3 mb-0 mx-3">
			<strong>{!! $error !!}</strong>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
	@endforeach
@endif
@if (Session::has('status') && Session::has('notify'))
	<div class="alert alert-{!! Session::get('status') !!} alert-dismissible fade show mt-3 mb-0 mx-3">
		<strong>{!! Session::get('notify') !!}</strong>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
@endif
