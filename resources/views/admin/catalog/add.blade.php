@extends('admin.home')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-12 card_title">
							<h4>Thêm mới danh mục sản phẩm</h4>
						</div>
					</div>
				</div>
				@include('admin.notify')
				<div class="card-body">
					<form action="" method="post" accept-charset="utf-8">
						@csrf
						<div class="form-group row">
							<div class="col-md-6 pl-0 form_mac">
								<label class="label_mac">Name</label>
								<input type="text" name="name" class="form-control input_mac" value="{{ old('name') }}" required="" maxlength="200">
							</div>
							<div class="col-md-6 pr-0 form_mac">
								<label class="label_mac">Link</label>
								<input type="text" name="slug" class="form-control input_mac" value="{{ old('link') }}" disabled="">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-4 pl-0 form_mac">
								<select name="parent" class="form-control select_mac">
									<option value="0" >No Parent</option>
									@foreach ($list as $item)
										@if ($item->display == 1 && $item->parent_id == 0)
										<option value="{{ $item->id }}" >{{ $item->name }}</option>
										@endif
									@endforeach
								</select>
							</div>
							<div class="col-md-4 form_mac">
								<select name="display" class="form-control select_mac">
									<option value="0" >Hide</option>
									<option value="1" selected>Show</option>
								</select>
							</div>
							<div class="col-md-4 pr-0 form_mac">
								<select name="type" class="form-control select_mac">
									@foreach (config('product.type') as $item)
									<option value="{{ $item }}" selected>{{ $item }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group text-right">
							<button type="submit" class="btn">Thêm mới</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		
		$('[name="name"]').keyup(function() {
			$('[name="slug"]').parent('.form_mac').children('.label_mac').css({'top':'-20px', 'font-size':'11px', 'transition-duration':'0.3s'});
			slug = $('[name="name"]').val();
			slug = replace_vi(slug);
			slug = slug.replace(/\s/gi,'-');
			console.log(slug)
			$('[name="slug"]').val(slug);
		});
	});
</script>
@endsection