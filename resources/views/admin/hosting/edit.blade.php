@extends('admin.home')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-12 card_title">
							<h4>Cập nhật hosting</h4>
						</div>
					</div>
				</div>
				@include('admin.notify')
				<div class="card-body">
					<form action="" method="post" accept-charset="utf-8">
						@csrf
						<div class="form-group row">
							<div class="col-md-6 pl-0 form_mac">
								<label class="label_mac">Name</label>
								<input type="text" name="name" class="form-control input_mac" value="{{ $data->name }}" maxlength="255" required="">
							</div>
							<div class="col-md-6 pr-0 form_mac">
								<select name="catalog" class="form-control select_mac">
									<option value="">-- Select catalog --</option>
									@foreach ($catalog as $item)
									<option value="{{ @$item->id }}" class="option_mac" {{ @($item->id == $data->catalog) ? 'selected' : '' }}>{{ @$item->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6 pl-0 form_mac">
								<label class="label_mac">Fees</label>
								<input type="number" step="any" name="fees" class=" form-control input_mac" value="{{ $data->fees }}" maxlength="20">
							</div>
							<div class="col-md-6 pr-0 form_mac">
								<label class="label_mac">Periodic</label>
								<input type="number" step="any" name="periodic" class=" form-control input_mac" value="{{ $data->periodic }}" maxlength="11">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-4 pl-0 form_mac">
								<select name="display" class="form-control select_mac">
									<option value="1" class="option_mac" {{ @($data->display == 1) ? 'selected' : '' }}>Show</option>
									<option value="0" class="option_mac" {{ @($data->display == 0) ? 'selected' : '' }}>Hidden</option>
								</select>
							</div>
							<div class="col-md-4 form_mac">
								<label class="label_mac">Link</label>
								<input type="text" name="link" class="form-control input_mac" value="{{ @$data->link }}"  >
							</div>
							<div class="col-md-4 pr-0 form_mac">
								<label class="label_mac">Note</label>
								<textarea name="note" class="form-control input_mac" rows="1" maxlength="2000">{{ $data->note }}</textarea>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-4 pl-0 form_mac">
								<label class="label_mac">Storage</label>
								<input type="text" name="storage" class="form-control input_mac" value="{{ $data->storage }}" maxlength="100" >
							</div>
							<div class="col-md-4 form_mac">
								<label class="label_mac">Bandwidth</label>
								<input type="text" name="bandwidth" class="form-control input_mac" value="{{ $data->bandwidth }}" maxlength="100" >
							</div>
							<div class="col-md-4 pr-0 form_mac">
								<label class="label_mac">Domain</label>
								<input type="text" name="domain" class="form-control input_mac" value="{{ $data->domain }}" maxlength="100" >
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-4 pl-0 form_mac">
								<label class="label_mac">Subdomain</label>
								<input type="text" name="sub_domain" class="form-control input_mac" value="{{ $data->sub_domain }}" maxlength="100" >
							</div>
							<div class="col-md-4 form_mac">
								<label class="label_mac">Packed Domain</label>
								<input type="text" name="pack_domain" class="form-control input_mac" value="{{ $data->pack_domain }}" maxlength="100" >
							</div>
							<div class="col-md-4 pr-0 form_mac">
								<label class="label_mac">Addon Domain</label>
								<input type="text" name="addon_domain" class="form-control input_mac" value="{{ $data->addon_domain }}" maxlength="100" >
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-4 pl-0 form_mac">
								<label class="label_mac">Database</label>
								<input type="number" step="any" name="database" class="form-control input_mac" value="{{ $data->database }}" maxlength="11" >
							</div>
							<div class="col-md-4 form_mac">
								<label class="label_mac">FTP Account</label>
								<input type="number" step="any" name="ftp_account" class="form-control input_mac" value="{{ $data->ftp_account }}" maxlength="11" >
							</div>
							<div class="col-md-4 pr-0 form_mac">
								<label class="label_mac">Email</label>
								<input type="number" step="any" name="email" class="form-control input_mac" value="{{ $data->email }}" maxlength="11" >
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-4 pl-0 form_mac">
								<label class="label_mac">SSL</label>
								<input type="text" name="ssl" class="form-control input_mac" value="{{ $data->ssl }}" maxlength="100" >
							</div>
							<div class="col-md-4 form_mac">
								<label class="label_mac">IP</label>
								<input type="number" step="any" name="ip" class="form-control input_mac" value="{{ $data->ip }}" maxlength="11" >
							</div>
							<div class="col-md-4 pr-0 form_mac">
								<label class="label_mac">Service</label>
								<textarea name="services" class="form-control input_mac" rows="1" maxlength="2000">{{ $data->services }}</textarea>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12 px-0 form_mac">
								@foreach (config('location.product') as $item)
								<div class="custom-control custom-checkbox d-inline pr-5">
									<input type="checkbox" class="custom-control-input" name="location[]" value="{{ $item }}" id="{{ $item }}_checkbox" {{ (in_array($item, json_decode($data->location))) ? 'checked' : '' }}>
									<label class="custom-control-label" for="{{ $item }}_checkbox">{{ strtoupper($item) }}</label>
								</div>
								@endforeach
							</div>
						</div>
						<div class="form-group text-right">
							<button type="submit" class="btn">Cập nhật</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection