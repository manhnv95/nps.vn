@extends('admin.home')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-12 card_title">
							<h4>Cập nhật SEO</h4>
						</div>
					</div>
				</div>
				@include('admin.notify')
				<div class="card-body">
					<form action="" method="post" accept-charset="utf-8">
						@csrf
						<div class="form-group row">
							<div class="form_mac col-md-12">
								<label class="label_mac">Name</label>
								<input type="text" name="name" class="form-control input_mac" value="{{ $data->name }}" maxlength="255" required="">
							</div>
						</div>
						<div class="form-group row">
							<div class="form_mac col-md-12">
								<label class="">Value</label>
								<textarea name="value" class="form-control" rows="10" >{{ $data->value }}</textarea>
							</div>
						</div>
						<div class="form-group text-right">
							<button type="submit" class="btn">Cập nhật</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection