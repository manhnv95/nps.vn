@php
	use App\Http\Controllers\Helper\MAC;
@endphp
@extends('admin.home')
@section('content')
<div class="container-fluid">
	<div class="row">
		@include('admin.notify')
		@foreach ($config_widget as $key => $element)
		<div class="col-md-4 pb-5">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-12 card_title">
							<h4>{{ $element['label'] }}</h4>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-hover table-striped">
							<tbody>
								@foreach ($list as $item)
								@if (in_array($item->name, $element['data']))
								<tr>
									<td><a href="{{ route('admin.config.show_homepage') }}" title="">{{ $item->label }}</a></td>
									<td class="text-right"><a href="{{ route('admin.config.edit_homepage', ['id' => $item->id]) }}" title=""><i class="fas fa-edit"></i></a></td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>
@endsection