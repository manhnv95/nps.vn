@extends('admin.home')
@section('content')
<script src="/js/ckeditor.js"></script>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-9 card_title">
							<h4>Cập nhật giá trị {{ $data->label }}</h4>
						</div>
						<div class="col-md-3">
							<a class="btn btn-light" href="{{ route('admin.config.show_homepage') }}" title="">Back to Config Homepage</a>
						</div>
					</div>
				</div>
				@include('admin.notify')
				<div class="card-body">
					<form action="" method="post" accept-charset="utf-8">
						@csrf
						<div class="form-group row">
							<div class="col-md-6 pl-0 form_mac">
								<label class="label_mac">Name</label>
								<input type="text" name="name" class="form-control input_mac" value="{{ $data->label }}" maxlength="250" required="">
							</div>
							<div class="col-md-6 pr-0 form_mac">
								<select name="type" class="form-control select_mac" disabled="">
									<option value="label" class="option_mac" {{ @($data->type == 'label') ? 'selected' : '' }}>Label</option>
									<option value="link" class="option_mac" {{ @($data->type == 'link') ? 'selected' : '' }}>Link</option>
									<option value="text" class="option_mac" {{ @($data->type == 'text') ? 'selected' : '' }}>Text</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12 px-0 form_mac">
								@if ($data->type == 'text')
								<textarea id="editor" name="value" class="form-control input_mac" rows="1" maxlength="2000">{!! $data->value !!}</textarea>
								@else
								<label class="label_mac">Value</label>
								<input type="text" name="value" class=" form-control input_mac" value="{{ $data->value }}" maxlength="250">
								@endif
							</div>
						</div>
						<div class="form-group text-right">
							<button type="submit" class="btn">Cập nhật</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	ClassicEditor
        .create( document.querySelector( '#editor' ),{
        	toolbar: [
	            'heading', '|', 'custombutton', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote', 'link', 'alignment'
	        ]
        } )
        .catch( error => {
            console.error( error );
        } );
</script>
@endsection