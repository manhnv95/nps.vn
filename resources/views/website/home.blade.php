@php
    use App\Http\Controllers\Helper\MAC;
    $widget_home = MAC::getConfig('home');
@endphp
@extends('website.index')
@section('main')

{{-- --------------------- dịch vụ của NPS ------------------------ --}}
<div class="hm-container-fluid banner lazy">
    <div class="banner-title">
        <h3>{{ @$widget_home->support_title }}</h3>
    </div>
   <div class="hm-container">
        <div class="hm-row">
            <div class="hm-col-12">
                <div class="hm-column-4 hm-row">
                    <div class="item">
                        <div class="banner-icons">
                            <div class="banner-icon"><img src="{{ @$widget_home->support_img_1 }}" alt=""></div>
                        </div>     
                    </div>    
                    <div class="item">
                        <div class="banner-icons">
                            <div class="banner-icon"><img src="{{ @$widget_home->support_img_2 }}" alt=""></div>
                        </div>
                    </div>
                    <div class="item">
                         <div class="banner-icons">
                            <div class="banner-icon"><img src="{{ @$widget_home->support_img_3 }}" alt=""></div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="banner-icons">
                            <div class="banner-icon"><img src="{{ @$widget_home->support_img_4 }}" alt=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
</div>
<div  data-wow-duration="2s" id="our-product" class="wow hm-container-fluid lazy">
   <div class="hm-container">
        <div class="hm-row">
        <div class="hm-col-12">
            <div class="wrapper wrapper-product">
                <div class="product-title">
                    <h2>{{ @$widget_home->services_title }}</h2>
                </div>
                <div class="list-product hm-row hm-column-4">
                    <div class="item">
                        <div class="product">
                            <div class="img img-1-1 icon-product">
                                <img src="{{ @$widget_home->services_img_1 }}" alt="">
                            </div>
                            <div class="title">
                                <h3>{{ @$widget_home->services_title_1 }}</h3>
                            </div>
                            <p class="des">{!! @$widget_home->services_description_1 !!}</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="product">
                            <div class="img img-1-1 icon-product">
                                <img src="{{ @$widget_home->services_img_2 }}" alt="">
                            </div>
                            <div class="title">
                                <h3>{{ @$widget_home->services_title_2 }}</h3>
                            </div>
                            <p class="des">{!! @$widget_home->services_description_2 !!}</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="product">
                            <div class="img img-1-1 icon-product">
                                <img src="{{ @$widget_home->services_img_3 }}" alt="">
                            </div>
                            <div class="title">
                                <h3>{{ @$widget_home->services_title_3 }}</h3>
                            </div>
                            <p class="des">{!! @$widget_home->services_description_3 !!}</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="product">
                            <div class="img img-1-1 icon-product">
                                <img src="{{ @$widget_home->services_img_4 }}" alt="">
                            </div>
                            <div class="title">
                                <h3>{{ @$widget_home->services_title_4 }}</h3>
                            </div>
                            <p class="des">{!! @$widget_home->services_description_4 !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   </div>
</div>



{{-- --------------------- HOSTING ------------------ --}}
<div  data-wow-duration="2s" id="hosting" class="wow hm-container lazy">
    <div class="hm-row">
        <div class="hm-col-12">
            <div class="wrapper wrapper-hosting">
                <div class="price-title">
                    <h2>{{ @$widget_home->hosting_title }}</h2>
                </div>
                <div class="list-price hm-row hm-column-4 text-c">
                    @foreach ($hosting as $item)
                    @if (!empty(json_decode($item->location)))
                    @if (in_array('home', json_decode($item->location)))
                    <div class="item">
                        <div class="price">
                            <div class="title">
                                <h3>{{ $item->name }}</h3>
                            </div>
                            <div class="laptop">
                                <img src="website/images/img_product/hosting.webp" alt="">
                            </div>
                            <div class="total-price">{!! ($item->fees == '6969696969') ? 'Liên hệ' : number_format($item->fees).' <span>/'.$item->periodic.' tháng</span>' !!}</div>
                            <div class="hardware-info">
                                <p>{{ @$widget_home->hosting_panel_title }}</p>
                                <table class="des">
                                    <tr><td class="name">Dung lượng </td><td class="info">{{ $item->storage }}</td></tr>
                                    <tr><td class="name">Băng thông </td><td class="info">{{ $item->bandwidth }}</td></tr>
                                    <tr><td class="name">Tên miền </td><td class="info">{{ $item->domain }}</td></tr>
                                    <tr><td class="name">Tên miền phụ </td><td class="info">{{ $item->pack_domain }}</td></tr>
                                    <tr><td class="name">Cơ sở dữ liệu </td><td class="info">{{ $item->database }}</td></tr>
                                    <tr><td class="name">FTP Account </td><td class="info">{{ $item->ftp_account }}</td></tr>
                                </table>
                                <a href="{{ @$item->link }}">{{ @$widget_home->hosting_panel_button }}</a>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hm-container-fluid banner-1 lazy">
    <div class="hm-container">
        <div class="hm-row">
            <div class="hm-col-7">
                <h3>{{ @$widget_home->hosting_des_title }}</h3>
                <p>{!! @$widget_home->hosting_des_text !!}</p>
            </div>
            <div class="hm-col-5">
            <div class="hardware-banner img img-2-1">
                <img src="{{ @$widget_home->hosting_des_img }}" alt="">
            </div>
        </div>
        </div>
    </div>
</div>


{{-- --------------------- VPS ---------------------- --}}
<div  data-wow-duration="2s" id="vps" class="wow hm-container lazy">
    <div class="hm-row">
        <div class="hm-col-12">
            <div class="wrapper wrapper-vps">
                <div class="vps-title"><h2>{{ @$widget_home->vps_title }}</h2></div>
                <div class="list-vps hm-row hm-column-4 text-c">
                    @foreach ($vps as $item)
                    @if (!empty(json_decode($item->location)))
                    @if (in_array('home', json_decode($item->location)))
                    <div class="item">
                        <div class="vps">
                            <div class="price">
                                {{ ($item->fees == '6969696969') ? 'Liên hệ' : number_format($item->fees).' / '.$item->periodic.' tháng'  }}
                            </div>
                            <div class="hardware-vps"><img src="website/images/img_product/vps.webp" alt=""></div>
                            <div class="vps-info">
                                <div class="price">
                                    {{ ($item->fees == '6969696969') ? 'Liên hệ' : number_format($item->fees).' / '.$item->periodic.' tháng'  }}
                                </div>
                                <table class="des">
                                    <tr><td class="name">CPU </td><td class="info" colspan="2">{{ $item->cpu }}</td></tr>
                                    <tr><td class="name">RAM </td><td class="info" colspan="2">{{ $item->ram }}</td></tr>
                                    <tr><td class="name">Disk </td><td class="info" colspan="2">{{ $item->storage.' '.strtoupper($item->disk) }}</td></tr>
                                    <tr><td class="name">Speed </td><td class="info" colspan="2">{{ $item->speed }}</td></tr>
                                    <tr><td class="name">Services </td><td class="info" colspan="2">{{ $item->services }}</td></tr>
                                    <tr><td class="name">Bandwidth </td><td class="info" colspan="2">{{ $item->bandwidth }}</td></tr>
                                </table>
                                <a href="{{ @$item->link }}">{{ @$widget_home->vps_panel_button }}</a>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hm-container-fluid banner-1 lazy">
    <div class="hm-container">
        <div class="hm-row">
            <div class="hm-col-7">
                <h3>{{ @$widget_home->vps_des_title }}</h3>
                <p>{!! @$widget_home->vps_des_text !!}</p>
            </div>
            <div class="hm-col-5">
                <div class="hardware-banner img img-2-1">
                    <img src="{{ @$widget_home->vps_des_img }}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>


{{-- -------------------- THUÊ SERVER ------------------------- --}}
<div  data-wow-duration="2s" id="server" class="wow hm-container lazy">
    <div class="wrapper wrapper-server">
        <div class="server-title"><h2>{{ @$widget_home->rentsv_title }}</h2></div>
        <div class="list-server hm-row hm-column-4 text-c">
            @foreach ($dedicated as $item)
            @if (!empty(json_decode($item->location)))
            @if (in_array('home', json_decode($item->location)))
            @if ($item->type == 'rent_server')
            <div class="item">
                <div class="server">
                    <div class="server-img">
                        <img src="website/images/img_product/rent_server.webp" alt="">
                    </div>
                    <div class="price">{{ ($item->fees == '6969696969') ? 'Liên hệ' : number_format($item->fees).' / '.$item->periodic.' tháng'  }}</div>
                    <div class="hover-server">
                        <h3 class="server-name">{{ $item->name }}</h3>
                        <table class="des">
                            <tr><td class="name">CPU </td><td class="info">{{ $item->cpu }}</td></tr>
                            <tr><td class="name">RAM </td><td class="info">{{ $item->ram }}</td></tr>
                            <tr><td class="name">Disk </td><td class="info">{{ $item->storage.' '.$item->disk }}</td></tr>
                            <tr><td class="name">Speed </td><td class="info">{{ $item->speed }}</td></tr>
                            <tr><td class="name">Băng thông </td><td class="info">{{ $item->bandwidth }}</td></tr>
                            <tr><td class="name">Dịch vụ </td><td class="info">{{ $item->services }}</td></tr>
                        </table>
                        <a href="{{ @$item->link }}">{{ @$widget_home->rentsv_panel_button }}</a>
                    </div>
                </div>
            </div>
            @endif
            @endif
            @endif
            @endforeach
        </div>
    </div>
</div>
<div class="hm-container-fluid banner-1 lazy">
    <div class="hm-container">
        <div class="hm-row">
            <div class="hm-col-7">
                <h3>{{ @$widget_home->rentsv_des_title }}</h3>
                <p>{!! @$widget_home->rentsv_des_text !!}</p>
            </div>
            <div class="hm-col-5">
                <div class="hardware-banner img img-2-1">
                    <img src="{{ @$widget_home->rentsv_des_img }}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>



{{-- -------------------------- FIREWALL ---------------- --}}
<div  data-wow-duration="2s" id="firewall" class="wow hm-container lazy">
    <div class="hm-row">
        <div class="hm-col-12">
            <div class="wrapper wrapper-firewall">
                <div class="firewall-title">
                    <h2>{{ @$widget_home->nwall_title }}</h2>
                </div>
                <div class="list-firewall hm-row hm-column-4 text-c">
                    @foreach ($nwall as $item)
                    @if (!empty(json_decode($item->location)))
                    @if (in_array('home', json_decode($item->location)))
                    <div class="item">
                        <div class="firewall">
                            <div class="before-firewall">
                                <div class="firewall-img"><img src="website/images/img_product/firewall.webp" alt=""></div>
                                <div class="info">
                                    <h3 class="title">{{ @$item->name }}</h3>
                                    <p class="price">{{ ($item->fees == '6969696969') ? 'Liên hệ' : number_format($item->fees).' / '.$item->periodic.' tháng'  }}</p>
                                </div>
                            </div>
                            <div class="after-firewall">
                                <p class="price">{{ ($item->fees == '6969696969') ? 'Liên hệ' : number_format($item->fees).' / '.$item->periodic.' tháng'  }}</p>
                                <a href="{{ @$item->link }}">{{ @$widget_home->nwall_panel_button }}</a>
                                <table class="des">
                                    <tr><td class="name">Connection </td><td class="info">{{ $item->connection }}</td></tr>
                                    <tr><td class="name">Rule </td><td class="info">{{ $item->rule }}</td></tr>
                                    <tr><td class="name">Băng thông </td><td class="info">{{ $item->bandwidth }}</td></tr>
                                    <tr><td class="name">Dashboard </td><td class="info">{{ $item->dashboard }}</td></tr>
                                    <tr><td class="name">API </td><td class="info">{{ $item->api }}</td></tr>
                                    <tr><td class="name">Dịch vụ </td><td class="info">{{ $item->services }}</td></tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hm-container-fluid banner-1 lazy">
    <div class="hm-container">
        <div class="hm-row">
            <div class="hm-col-7">
                <h3>{{ @$widget_home->nwall_des_title }}</h3>
                <p>{!! @$widget_home->nwall_des_text !!}</p>
            </div>
            <div class="hm-col-5">
                <div class="hardware-banner img img-2-1">
                    <img src="{{ @$widget_home->nwall_des_img }}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>


{{-- -------------------------- BÁN SERVER ------------------------ --}}
<div  data-wow-duration="2s" id="sellserver" class="wow hm-container lazy">
    <div class="hm-row">
        <div class="hm-col-12">
            <div class="wrapper wrapper-sellserver">
                <div class="sellserver-title">
                    <h2>{{ @$widget_home->sellsv_title }}</h2>
                </div>
                <div class="list-sellserver hm-row hm-column-4 text-c">
                    @foreach ($dedicated as $item)
                    @if (!empty(json_decode($item->location)))
                    @if (in_array('home', json_decode($item->location)))
                    @if ($item->type == 'sell_server')
                    <div class="item">
                        <div class="sellserver">
                            <div class="before-sellserver">

                                <div class="sellserver-img"><img src="website/images/img_product/sell_server.webp" alt=""></div>
                                 <div class="info">
                                    <h3>{{ $item->name }}</h3>
                                    <p class="price">{{ ($item->fees == '6969696969') ? 'Liên hệ' : number_format($item->fees).' / '.$item->periodic.' tháng'  }}</p>
                                </div>
                            </div>
                            <div class="after-sellserver">
                                <p class="price">{{ ($item->fees == '6969696969') ? 'Liên hệ' : number_format($item->fees).' / '.$item->periodic.' tháng'  }}</p>
                                <a href="{{ @$item->link }}">{{ @$widget_home->sellsv_panel_button }}</a>
                                <table class="des">
                                    <tr><td class="name">Cpu </td><td class="info">{{ $item->cpu }}</td></tr>
                                    <tr><td class="name">RAM </td><td class="info">{{ $item->ram }}</td></tr>
                                    <tr><td class="name">Disk </td><td class="info">{{ $item->storage.' '.$item->disk }}</td></tr>
                                    <tr><td class="name">Speed </td><td class="info">{{ $item->speed }}</td></tr>
                                    <tr><td class="name">Băng thông </td><td class="info">{{ $item->bandwidth }}</td></tr>
                                    <tr><td class="name">Dịch vụ </td><td class="info">{{ $item->services }}</td></tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endif
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hm-container-fluid banner-1 lazy">
    <div class="hm-container">
        <div class="hm-row">
            <div class="hm-col-7">
                <h3>{{ @$widget_home->sellsv_des_title }}</h3>
                <p>{!! @$widget_home->sellsv_des_text !!}</p>
            </div>
            <div class="hm-col-5">
                <div class="hardware-banner img img-2-1">
                    <img src="{{ @$widget_home->sellsv_des_img }}" alt="">
                </div>
            </div>
        </div>

    </div>
</div>


{{-- ----------------------- CHỖ ĐẶT SERVER ------------------- --}}
<div  data-wow-duration="2s" id="serverplace" class="wow hm-container lazy">
    <div class="wrapper wrapper-serverplace">
        <div class="serverplace-title"><h2>{{ @$widget_home->colocation_title }}</h2></div>
        <div class="list-serverplace hm-row hm-column-2 text-c">
            @foreach ($colocation as $item)
            @if (!empty(json_decode($item->location)))
            @if (in_array('home', json_decode($item->location)))
            <div class="item">
                <div class="serverplace">
                    <div class="serverplace-img">
                        <img src="website/images/img_product/cho_dat_server.webp" alt="">
                    </div>
                    <div class="first-des">
                        <h2>{{ $item->name }}</h2>
                        <p>{{ ($item->fees == '6969696969') ? 'Liên hệ' : number_format($item->fees).' / '.$item->periodic.' tháng'  }}</p>
                        <table class="des">
                            <tr><td class="name">Rack </td><td class="info">{{ $item->rack }}</td></tr>
                            <tr><td class="name">Tốc độ trong nước </td><td class="info">{{ $item->speed_domestic }}</td></tr>
                            <tr><td class="name">Tốc độ quốc tế </td><td class="info">{{ $item->speed_international }}</td></tr>
                            <tr><td class="name">IP </td><td class="info">{{ $item->ip }}</td></tr>
                            <tr><td class="name">Dịch vụ </td><td class="info">{{ $item->services }}</td></tr>
                        </table>
                        <a href="{{ @$item->link }}">{{ @$widget_home->colocation_panel_button }}</a>
                    </div>
                    {{-- <div class="price"></div> --}}
                    {{-- <div class="hover-serverplace">
                        <h3 class="serverplace-name">{{ $item->name }}</h3>
                        <table class="des">
                            <tr><td class="name">Rack </td><td class="info">{{ $item->rack }}</td></tr>
                            <tr><td class="name">Tốc độ trong nước </td><td class="info">{{ $item->speed_domestic }}</td></tr>
                            <tr><td class="name">Tốc độ quốc tế </td><td class="info">{{ $item->speed_international }}</td></tr>
                            <tr><td class="name">IP </td><td class="info">{{ $item->ip }}</td></tr>
                            <tr><td class="name">Dịch vụ </td><td class="info">{{ $item->services }}</td></tr>
                        </table>
                        <a href="{{ @$item->link }}">{{ @$widget_home->colocation_panel_button }}</a>
                    </div> --}}
                </div>
            </div>
            @endif
            @endif
            @endforeach
        </div>
    </div>
</div>
<div class="hm-container-fluid banner-1 lazy">
    <div class="hm-container">
        <div class="hm-row">
            <div class="hm-col-7">
                <h3>{{ @$widget_home->colocation_des_title }}</h3>
                <p>{!! @$widget_home->colocation_des_text !!}</p>
            </div>
            <div class="hm-col-5">
                <div class="hardware-banner img img-2-1">
                    <img src="{{ @$widget_home->colocation_des_img }}" alt="">
                </div>
            </div>
        </div>

    </div>
</div>



{{-- --------------------------- TỦ RACK ---------------------- --}}
<div  data-wow-duration="2s" id="rack" class="wow hm-container lazy">
    <div class="wrapper wrapper-rack">
        <div class="rack-title"><h2>{{ @$widget_home->rack_title }}</h2></div>
        <div class="list-rack hm-row hm-column-2 text-c">
            @foreach ($rack as $item)
            @if (!empty(json_decode($item->location)))
            @if (in_array('home', json_decode($item->location)))
            <div class="item">
                <div class="rack">
                    <div class="first-des">
                        <h3 class="rack-name">{{ $item->name }}</h3>
                        <div class="price">{{ ($item->fees == '6969696969') ? 'Liên hệ' : number_format($item->fees).' / '.$item->periodic.' tháng'  }}</div>
                        <table class="des">
                            <tr><td class="name">Rack</td> <td class="info">{{ $item->rack }}</td></tr>
                            <tr><td class="name">Tốc độ trong nước</td><td class="info">{{ $item->speed_domestic }}</td></tr>
                            <tr><td class="name">Tốc độ quốc tế</td><td class="info">{{ $item->speed_international }}</td></tr>
                            <tr><td class="name">IP</td><td class="info">{{ $item->ip }}</td></tr>
                            <tr><td class="name">Băng thông</td><td class="info">{{ $item->bandwidth }}</td></tr>
                            <tr><td class="name">Dịch vụ</td><td class="info">{{ $item->services }}</td></tr>
                        </table>
                        <a href="{{ @$item->link }}">{{ @$widget_home->rack_panel_button }}</a>
                    </div>
                    <div class="rack-img">
                        <img src="website/images/img_product/rack.webp" alt="">
                    </div>


                    {{-- <div class="hover-rack">
                        <h3 class="rack-name">{{ $item->name }}</h3>
                        <table class="des">
                            <tr><td class="name">Rack</td> <td class="info">{{ $item->rack }}</td></tr>
                            <tr><td class="name">Tốc độ trong nước</td><td class="info">{{ $item->speed_domestic }}</td></tr>
                            <tr><td class="name">Tốc độ quốc tế</td><td class="info">{{ $item->speed_international }}</td></tr>
                            <tr><td class="name">IP</td><td class="info">{{ $item->ip }}</td></tr>
                            <tr><td class="name">Băng thông</td><td class="info">{{ $item->bandwidth }}</td></tr>
                            <tr><td class="name">Dịch vụ</td><td class="info">{{ $item->services }}</td></tr>
                        </table>
                        <a href="{{ @$item->link }}">{{ @$widget_home->rack_panel_button }}</a>
                    </div> --}}
                </div>
            </div>
            @endif
            @endif
            @endforeach
        </div>
    </div>
</div>
<div class="hm-container-fluid plr-0 lazy">
    <div class="banner-2 img">
        <div class="hm-container">
            <div class="des">
                <h3>{{ @$widget_home->rack_des_title }}</h3>
                <p>{!! @$widget_home->rack_des_text !!}</p>
            </div>
        </div>
        <img src="{{ @$widget_home->rack_des_img }}">
    </div>
</div>
@endsection