@extends('website.index')
@section('main')
<div class="hm-container-fluid banner-1 post">
    <div class="hm-container">
        <div class="hm-row va-m">
            <div class="hm-col-12">
                <p>{!! $page_data->content !!}</p>
            </div>
        </div>
    </div>
</div>
<div class="hm-container">
    <div class="hm-row">
        <div class="hm-col-12 text-c">
            <div class="wrapper wrapper-hosting" style="margin-bottom: 70px;">
                <div class="price-title">
                    <h2>Bảng Giá</h2>
                    <p style="font-weight: 700;text-transform: uppercase; font-size:20px; color: #3eb5a7">Đã bao gồm Default AntiDDOS</p>
                </div>
                <div class="list-price hm-row hm-column-4 text-c">
                    @foreach ($products as $item)
                    @if (!empty(json_decode($item->location)))
                    @if (in_array('page', json_decode($item->location)))
                    <div class="item">
                        <div class="price">
                            <div class="title">
                                <h3>{{ $item->name }}</h3>
                            </div>
                            <div class="laptop">
                                <img src="website/images/img_product/hosting.png" alt="">
                            </div>
                            <div class="total-price">{!! ($item->fees == '6969696969') ? 'Liên hệ' : number_format($item->fees).' <span>/'.$item->periodic.' tháng</span>' !!}</div>
                            <div class="hardware-info">
                                <p>Thông số kỹ thuật</p>
                                <table class="des">
                                    @foreach (config('page.parameter.'.$parameter) as $key => $val)
                                    @if (!empty($item->$key))
                                        <tr><td class="name">{{ $val }}</td> : <td class="info">{{ $item->$key }}</td></tr>
                                    @endif
                                    @endforeach
                                </table>
                                <a href="{{ $item->link }}">Mua ngay</a>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection