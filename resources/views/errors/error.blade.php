<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $status }}</title>
</head>
<style>
*{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}
h1{
    text-transform: uppercase;
    font-size: 40px;
    text-align: center;
    color: #555;
}
a{
    text-decoration: none;
    margin: 0 auto;
    display: block;
    color: #555;
    font-size: 32px;
    border: 1px solid #555;
    width: fit-content;
    padding: 10px 20px;
    border-radius: 5px;
    text-transform: uppercase;
    background-color: #eee;
    text-align: center;
    transition: all .3s ease;
}
a:hover{
    color: #eee;
    background-color: #555;
}
.container{
    position: fixed;
    bottom: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}


</style>
<body>
    <div class="container">
        <h1>{{ $status }} <br> Error</h1>
        <a href="{{ route('home') }}">click here to back</a>
    </div>
</body>
</html>