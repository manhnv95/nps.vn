<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{
    public function logout()
    {
    	if (Auth::guest()) {
    		return redirect('/mlogin');
    	}else{
    		Auth::logout();
    		return redirect()->back();
    	}
    }
}
