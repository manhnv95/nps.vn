<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class ConfigController extends Controller
{
    public function index()
    {
    	return view('admin.config.index');
    }

    public function show_homepage()
    {
    	$config_data = [];
    	$config_name = [];
        $config_page = [];
    	$data = [];
        foreach (config('widget') as $item) {
            foreach ($item as $key => $val) {
                $config_page[$key] = $val;
            }
        }
    	foreach ($config_page as $item => $element) {
    		$config_widget[$item]['label'] = $element['label'];
    		foreach ($element['data'] as $key => $val) {
    			$config_data[] = $val;
    			$config_widget[$item]['data'][$key] = $val['name'];
    		}
    	}
    	$list['config_widget'] = $config_widget;
    	$config_db = DB::table('config')->select('name')->get();
    	foreach ($config_db as $item) {
    		$config_name[] = $item->name;
    	}
    	foreach ($config_data as $item) {
    		if (!in_array($item['name'], $config_name)) {
    			$data[] = $item;
    		}
    	}
    	if (!empty($data)) {
    		DB::table('config')->insert($data);
    	}
    	$list['list'] = DB::table('config')->select('id', 'name', 'label')->get();
    	return view('admin.config.homepage', $list);
    }

    public function edit_homepage($id)
    {
    	$value = DB::table('config')->where('id', $id)->first();
    	if (!empty($value)) {
    		$data['data'] = $value;
    		return view('admin.config.edit_homepage', $data);
    	}
    	Session::flash('status', 'danger');
        Session::flash('notify', 'Không có giá trị này!');
        return redirect()->route('admin.config.homepage');
    }

    public function update_homepage(Request $request, $id)
    {
    	$value = DB::table('config')->where('id', $id)->first();
    	if (!empty($value)) {
    		$rules = [
	            'name'	=> 'required|max:250',
	        ];

	        $messages = config('lang.vi');
	        $this->validate($request, $rules, $messages);
    		$data = [
    			'label' => $request->name,
    			'value' => $request->value
    		];
    		DB::table('config')->where('id', $id)->update($data);
    		Session::flash('status', 'success');
	        Session::flash('notify', 'Cập nhật giá trị thành công!');
    		return redirect()->back();
    	}
    	Session::flash('status', 'danger');
        Session::flash('notify', 'Không có giá trị này!');
        return redirect()->route('admin.config.homepage');
    }
}
