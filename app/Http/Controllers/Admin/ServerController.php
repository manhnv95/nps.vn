<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class ServerController extends Controller
{
    public function index()
    {
        $data['list'] = DB::table('dedicated')->paginate(10);
        return view('admin.dedicated.index', $data);
    }

    public function create()
    {
        $data['catalog'] = DB::table('catalog')->select('id', 'name')->where('display', 1)->get();
        return view('admin.dedicated.add', $data);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:250|unique:dedicated,name',
        ];

        $messages = config('lang.vi');
        $this->validate($request, $rules, $messages);

        $date_time = date('Y-m-d H:i:s');
		$parent_id = (int) $request->parent;
        $catalog = (int) $request->catalog;
        $location = (!empty($request->location)) ? json_encode($request->location) : json_encode(['page']);
        $data = [
            'name'     => (string) $request->name,
            'catalog'  => $catalog,
            'type'      => $request->type,
            'fees'     => (int) $request->fees,
            'periodic' => (int) $request->periodic,
            'display'  => (int) $request->display,
            'note'     => (string) $request->note,
            'cpu'  => (string) $request->cpu,
            'ram'  => (string) $request->ram,
            'disk'  => (string) $request->disk,
            'storage'  => (string) $request->storage,
            'speed'  => (string) $request->speed,
            'bandwidth'     => (string) $request->bandwidth,
            'OS'        => (string) $request->os,
            'services'   => (string) $request->services,
            'link'   => (string) $request->link,
            'location'   => $location,
            'created_at' => $date_time,
            'updated_at' => $date_time,
        ];
        $id = DB::table('dedicated')->insertGetId($data);

        if ($id) {
            if ($catalog != 0) {
                DB::table('catalog')->where('id', $catalog)->increment('count_product');
            }
            Session::flash('status', 'success');
            Session::flash('notify', 'Thêm mới dedicated thành công!');
            return redirect()->route('admin.dedicated.edit', ['id' => $id]);
        }else{
            Session::flash('status', 'danger');
            Session::flash('notify', 'Thêm mới dedicated không thành công!');
            return redirect()->route('admin.dedicated.index');
        }
    }

    public function edit($id)
    {
        $dedicated = DB::table('dedicated')->where('id', $id)->first();
        if (isset($dedicated)) {
            $data['data'] = $dedicated;
            $data['catalog'] = DB::table('catalog')->select('id', 'name')->where('display', 1)->get();
            return view('admin.dedicated.edit', $data);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có dedicated này!');
        return redirect()->route('admin.dedicated.index');
    }

    public function update(Request $request, $id)
    {
       $dedicated = DB::table('dedicated')->where('id', $id)->first();
        if (isset($dedicated)) {
            $rules = [
                'name' => 'required|max:200',
            ];
            $messages = config('lang.vi');
            $date_time = date('Y-m-d H:i:s');
			$parent_id = (int) $request->parent;
            $catalog = (int) $request->catalog;
            $catalog_new = 0;
            if ($dedicated->catalog != $catalog) {
                $catalog_new = $catalog;
            }
            $location = (!empty($request->location)) ? json_encode($request->location) : json_encode(['page']);
            $data = [
	            'name'     => (string) $request->name,
	            'catalog'  => $catalog,
                'type'      => $request->type,
	            'fees'     => (int) $request->fees,
	            'periodic' => (int) $request->periodic,
	            'display'  => (int) $request->display,
	            'note'     => (string) $request->note,
	            'cpu'  => (string) $request->cpu,
	            'ram'  => (string) $request->ram,
	            'disk'  => (string) $request->disk,
	            'storage'  => (string) $request->storage,
	            'speed'  => (string) $request->speed,
	            'bandwidth'     => (string) $request->bandwidth,
	            'OS'        => (string) $request->os,
	            'services'   => (string) $request->services,
                'link'   => (string) $request->link,
                'location'   => $location,
	            'updated_at' => $date_time,
	        ];

            $this->validate($request, $rules, $messages);
            $function = DB::table('dedicated')->where('id', $id)->update($data);
            if ($function) {
            	if ($catalog_new != 0) {
		    		DB::table('catalog')->where('id', $catalog_new)->increment('count_product');
                    DB::table('catalog')->where('id', $dedicated->catalog)->decrement('count_product');
		    	}
                Session::flash('status', 'success');
                Session::flash('notify', 'Cập nhật thông tin dedicated thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Cập nhật thông tin dedicated không thành công!');
            }
            return redirect()->route('admin.dedicated.edit', ['id' => $id]);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có dedicated này!');
        return redirect()->route('admin.dedicated.index');
    }

    public function delete($id)
    {
        $dedicated = DB::table('dedicated')->where('id', $id)->first();
        if (isset($dedicated)) {
            $function = DB::table('dedicated')->where('id', $id)->delete();
            if ($function) {
                if ($dedicated->catalog != 0) {
                    DB::table('catalog')->where('id', $dedicated->catalog)->decrement('count_product');
                }
                Session::flash('status', 'success');
                Session::flash('notify', 'Xóa dedicated thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Xóa dedicated không thành công!');
            }
            return redirect()->route('admin.dedicated.index');
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có dedicated này!');
        return redirect()->route('admin.dedicated.index');
    }
}
