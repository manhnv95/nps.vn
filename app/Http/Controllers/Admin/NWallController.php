<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class NWallController extends Controller
{
    public function index()
    {
        $data['list'] = DB::table('nwall')->paginate(10);
        return view('admin.nwall.index', $data);
    }

    public function create()
    {
        $data['catalog'] = DB::table('catalog')->select('id', 'name')->where('display', 1)->get();
        return view('admin.nwall.add', $data);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:250|unique:nwall,name',
        ];

        $messages = config('lang.vi');
        $this->validate($request, $rules, $messages);

        $date_time = date('Y-m-d H:i:s');
		$parent_id = (int) $request->parent;
        $catalog = (int) $request->catalog;
        $location = (!empty($request->location)) ? json_encode($request->location) : json_encode(['page']);
        $data = [
            'name'     	=> (string) $request->name,
            'catalog'  	=> $catalog,
            'fees'     	=> (int) $request->fees,
            'periodic' 	=> (int) $request->periodic,
            'display'  	=> (int) $request->display,
            'note'     	=> (string) $request->note,
            'connection'	=> (int) $request->connection,
            'rule'  	=> (int) $request->rule,
            'bandwidth' => (string) $request->bandwidth,
            'dashboard' => (string) $request->dashboard,
            'api'  		=> (string) $request->api,
            'services'  => (string) $request->services,
            'link'   => (string) $request->link,
            'location'   => $location,
            'created_at'=> $date_time,
            'updated_at'=> $date_time,
        ];
        $id = DB::table('nwall')->insertGetId($data);

        if ($id) {
            if ($catalog != 0) {
                DB::table('catalog')->where('id', $catalog)->increment('count_product');
            }
            Session::flash('status', 'success');
            Session::flash('notify', 'Thêm mới nwall thành công!');
            return redirect()->route('admin.nwall.edit', ['id' => $id]);
        }else{
            Session::flash('status', 'danger');
            Session::flash('notify', 'Thêm mới nwall không thành công!');
            return redirect()->route('admin.nwall.index');
        }
    }

    public function edit($id)
    {
        $nwall = DB::table('nwall')->where('id', $id)->first();
        if (isset($nwall)) {
            $data['data'] = $nwall;
            $data['catalog'] = DB::table('catalog')->select('id', 'name')->where('display', 1)->get();
            return view('admin.nwall.edit', $data);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có nwall này!');
        return redirect()->route('admin.nwall.index');
    }

    public function update(Request $request, $id)
    {
       $nwall = DB::table('nwall')->where('id', $id)->first();
        if (isset($nwall)) {
            $rules = [
                'name' => 'required|max:200',
            ];
            $messages = config('lang.vi');
            $date_time = date('Y-m-d H:i:s');
			$parent_id = (int) $request->parent;
            $catalog = (int) $request->catalog;
            $catalog_new = 0;
            if ($nwall->catalog != $catalog) {
                $catalog_new = $catalog;
            }
            $location = (!empty($request->location)) ? json_encode($request->location) : json_encode(['page']);
            $data = [
	            'name'     	=> (string) $request->name,
	            'catalog'  	=> $catalog,
	            'fees'     	=> (int) $request->fees,
	            'periodic' 	=> (int) $request->periodic,
	            'display'  	=> (int) $request->display,
	            'note'     	=> (string) $request->note,
	            'connection'	=> (int) $request->connection,
	            'rule'  	=> (int) $request->rule,
	            'bandwidth' => (string) $request->bandwidth,
	            'dashboard' => (string) $request->dashboard,
	            'api'  		=> (string) $request->api,
	            'services'  => (string) $request->services,
                'link'   => (string) $request->link,
                'location'   => $location,
	            'updated_at'=> $date_time,
	        ];

            $this->validate($request, $rules, $messages);
            $function = DB::table('nwall')->where('id', $id)->update($data);
            if ($function) {
            	if ($catalog_new != 0) {
		    		DB::table('catalog')->where('id', $catalog_new)->increment('count_product');
                    DB::table('catalog')->where('id', $nwall->catalog)->decrement('count_product');
		    	}
                Session::flash('status', 'success');
                Session::flash('notify', 'Cập nhật thông tin nwall thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Cập nhật thông tin nwall không thành công!');
            }
            return redirect()->route('admin.nwall.edit', ['id' => $id]);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có nwall này!');
        return redirect()->route('admin.nwall.index');
    }

    public function delete($id)
    {
        $nwall = DB::table('nwall')->where('id', $id)->first();
        if (isset($nwall)) {
            $function = DB::table('nwall')->where('id', $id)->delete();
            if ($function) {
                if ($nwall->catalog != 0) {
                    DB::table('catalog')->where('id', $nwall->catalog)->decrement('count_product');
                }
                Session::flash('status', 'success');
                Session::flash('notify', 'Xóa nwall thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Xóa nwall không thành công!');
            }
            return redirect()->route('admin.nwall.index');
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có nwall này!');
        return redirect()->route('admin.nwall.index');
    }
}
