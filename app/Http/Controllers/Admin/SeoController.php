<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class SeoController extends Controller
{
    public function index()
    {
    	$data['list'] = DB::table('seo')->paginate(10);
    	return view('admin.seo.index', $data);
    }

    public function create()
    {
    	return view('admin.seo.add');
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:250|unique:seo,name',
        ];

        $messages = config('lang.vi');
        $this->validate($request, $rules, $messages);

        $date_time = date('Y-m-d H:i:s');
        $data = [
            'name'     	 => (string) $request->name,
            'value'  	 => $request->value,
        ];
        $id = DB::table('seo')->insertGetId($data);

        if ($id) {
            Session::flash('status', 'success');
            Session::flash('notify', 'Thêm mới thành công!');
            return redirect()->route('admin.seo.edit', ['id' => $id]);
        }else{
            Session::flash('status', 'danger');
            Session::flash('notify', 'Thêm mới không thành công!');
            return redirect()->route('admin.seo.index');
        }
    }

    public function edit($id)
    {
        $seo = DB::table('seo')->where('id', $id)->first();
        if (isset($seo)) {
            $data['data'] = $seo;
            return view('admin.seo.edit', $data);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có giá trị này!');
        return redirect()->route('admin.seo.index');
    }

    public function update(Request $request, $id)
    {
       $seo = DB::table('seo')->where('id', $id)->first();
        if (isset($seo)) {
            $rules = [
                'name' => 'required|max:200',
            ];
            $messages = config('lang.vi');
            $data = [
	            'name'     => (string) $request->name,
                'value'    => $request->value,
	        ];

            $this->validate($request, $rules, $messages);
            $function = DB::table('seo')->where('id', $id)->update($data);
            if ($function) {
                Session::flash('status', 'success');
                Session::flash('notify', 'Cập nhật thông tin thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Cập nhật thông tin không thành công!');
            }
            return redirect()->route('admin.seo.edit', ['id' => $id]);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có giá trị này!');
        return redirect()->route('admin.seo.index');
    }

    public function delete($id)
    {
        $seo = DB::table('seo')->where('id', $id)->first();
        if (isset($seo)) {
            $function = DB::table('seo')->where('id', $id)->delete();
            if ($function) {
                Session::flash('status', 'success');
                Session::flash('notify', 'Xóa tủ giá trị thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Xóa tủ giá trị không thành công!');
            }
            return redirect()->route('admin.seo.index');
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có giá trị này!');
        return redirect()->route('admin.seo.index');
    }
}
