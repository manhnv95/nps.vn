<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Hash;


class AdminController extends Controller
{
    public function index()
    {
        $data['list'] = DB::table('users')->where('roles', 'like', '%admin%')->where('status', '!=', 'deleted')->paginate(10);
        return view('admin.admin.index', $data);
    }

    public function create()
    {
        return view('admin.admin.add');
    }

    public function store(Request $request)
    {
        $rules = [
            'username'              => 'required|max:50|unique:users,name',
            'email'                 => 'required|max:50|unique:users,email',
            'password'              => 'required|min:8|max:16',
            'password_confirmation' => 'same:password',
            'fullname'              => 'required|max:200',
            'phone'                 => 'required|max:12',
            'address'               => 'max:200',
            'company'               => 'max:200',
            'country'               => 'max:200',
            'description'           => 'max:2000',
        ];

        $messages = config('lang.vi');
        $this->validate($request, $rules, $messages);

        $date_time = date('Y-m-d H:i:s');
        $user = [
            'name'      => $request->username,
            'email'     => $request->email,
            'password'  => Hash::make($request->password),
            'fullname'  => $request->fullname,
            'phone'     => $request->phone,
            'address'   => $request->address,
            'company'   => $request->company,
            'country'   => $request->country,
            'description' => $request->description,
            'created_at' => $date_time,
            'updated_at' => $date_time,
        ];
        $role = $request->role;
        if (array_key_exists($role, config('role'))) {
            $user['roles'] = json_encode([$role]);
        }

        $id = DB::table('users')->insertGetId($user);

        if ($id) {
            Session::flash('status', 'success');
            Session::flash('notify', 'Thêm mới quản trị viên thành công!');
            return redirect()->route('admin.admin.edit', ['id' => $id]);
        }else{
            Session::flash('status', 'danger');
            Session::flash('notify', 'Thêm mới quản trị viên không thành công!');
            return redirect()->route('admin.admin.index');
        }
    }

    public function edit($id)
    {
        $user = DB::table('users')->where('id', $id)->where('status', '!=', 'deleted')->first();
        if (isset($user)) {
            $data['user'] = $user;
            return view('admin.admin.edit', $data);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có người dùng này!');
        return redirect()->route('admin.admin.index');
    }

    public function update(Request $request, $id)
    {
        $user = DB::table('users')->where('id', $id)->where('status', '!=', 'deleted')->first();
        if (isset($user)) {
            $rules = [
                'username'              => 'required|max:50',
                'email'                 => 'required|max:50',
                'fullname'              => 'required|max:200',
                'phone'                 => 'required|max:12',
                'address'               => 'max:200',
                'company'               => 'max:200',
                'country'               => 'max:200',
                'description'           => 'max:2000',
            ];
            $messages = config('lang.vi');

            $date_time = date('Y-m-d H:i:s');
            $user = [
                'name'      => $request->username,
                'email'     => $request->email,
                'fullname'  => $request->fullname,
                'phone'     => $request->phone,
                'address'   => $request->address,
                'status'    => $request->status,
                'company'   => $request->company,
                'country'   => $request->country,
                'description' => $request->description,
                'updated_at' => $date_time,
            ];
            $status = $request->status;
            if (!in_array($status, config('user_status'))) {
                Session::flash('status', 'danger');
                Session::flash('notify', 'Vui lòng kiểm tra lại thông tin!');
                return redirect()->route('admin.admin.edit', ['id' => $id]);
            }

            $role = $request->role;
            if (array_key_exists($role, config('role'))) {
                $user['roles'] = json_encode([$role]);
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Vui lòng kiểm tra lại thông tin!');
                return redirect()->route('admin.admin.edit', ['id' => $id]);
            }

            $change_pass = $request->change_pass;
            if ($change_pass == 1) {
                $rules['password'] = 'required|min:8|max:16';
                $rules['password_confirmation'] = 'same:password';
                $user['password'] = Hash::make($request->password);
            }
            $this->validate($request, $rules, $messages);
            $function = DB::table('users')->where('id', $id)->update($user);
            if ($function) {
                Session::flash('status', 'success');
                Session::flash('notify', 'Cập nhật thông tin quản trị viên thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Cập nhật thông tin quản trị viên không thành công!');
            }
            return redirect()->route('admin.admin.edit', ['id' => $id]);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có quản trị viên này!');
        return redirect()->route('admin.admin.index');
    }

    public function delete($id)
    {
        $user = DB::table('users')->where('id', $id)->where('status', '!=', 'deleted')->first();
        if (isset($user)) {
            $function = DB::table('users')->where('id', $id)->update(['status' => 'deleted']);
            if ($function) {
                Session::flash('status', 'success');
                Session::flash('notify', 'Xóa quản trị viên thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Xóa quản trị viên không thành công!');
            }
            return redirect()->route('admin.admin.index');
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có người dùng này!');
        return redirect()->route('admin.admin.index');
    }
}
