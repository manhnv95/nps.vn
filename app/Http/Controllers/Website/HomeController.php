<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        $data['hosting'] = DB::table('hosting')->where('display', 1)->get();
        $data['vps'] = DB::table('vps')->where('display', 1)->get();
        $data['dedicated'] = DB::table('dedicated')->where('display', 1)->get();
        $data['nwall'] = DB::table('nwall')->where('display', 1)->get();
        $data['rack'] = DB::table('rack')->where('display', 1)->get();
        $data['colocation'] = DB::table('colocation')->where('display', 1)->get();

        return view('website.home', $data);
    }

    public function page($slug)
    {
        $page = DB::table('pages')->where('slug', $slug)->where('display', 1)->first();
        if (!empty($page)) {
            $data = [];
            if ($page->type == 'product') {
                $data['page_data'] = $page;
                if (empty($page->taxonomy)) {
                    return redirect()->route('home');
                }
                $catalog = DB::table('catalog')->where('id', $page->taxonomy)->select('id', 'name', 'type')->first();
                $data['products'] = DB::table($catalog->type)->where('display', 1)->where('catalog', $catalog->id)->get();
                $data['page_title'] = $page->page_title;
                $data['parameter'] = $catalog->type;
            }else if($page->type == 'taxonomy'){
                if (!empty($page->link)) {
                    return redirect($page->link);
                }
                return redirect()->route('home');
            }else if ($page->type == 'post') {
                $data['page_data'] = $page;
                $data['page_title'] = $page->page_title;
            }
            return view('website.pages.'.$page->html, $data);
        }
        return redirect()->route('home');
    }

    public function event()
    {
        return view('website.event');
    }

    public function bongda()
    {
        return view('website.bongda');
    }

    public function vnindo()
    {
        return view('website.vn_indo');
    }

}
