<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hosting', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('catalog');
            $table->integer('storage');
            $table->integer('bandwidth');
            $table->integer('domain');
            $table->integer('sub_domain');
            $table->integer('pack_domain');
            $table->integer('addon_domain');
            $table->integer('database');
            $table->integer('ftp_account');
            $table->integer('email');
            $table->integer('ssl');
            $table->integer('ip');
            $table->text('services');
            $table->double('fees');
            $table->integer('periodic');
            $table->boolean('display');
            $table->text('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hosting');
    }
}
