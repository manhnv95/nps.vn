<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDedicatedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dedicated', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('catalog');
            $table->integer('cpu');
            $table->integer('ram');
            $table->string('disk');
            $table->integer('storage');
            $table->integer('speed');
            $table->integer('bandwidth');
            $table->string('OS');
            $table->text('services');
            $table->double('fees');
            $table->integer('periodic');
            $table->boolean('display');
            $table->text('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dedicated');
    }
}
